package com.apis.controller;

import com.apis.Exception.ResourceNotFound;
import com.apis.dto.UserDto;
import com.apis.model.User;
import com.apis.dao.UserDao;
import com.apis.utils.Helper;
import com.constants.Constant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class UserController {

	@Autowired
	UserDao userRespository;
    private static final Logger log = LoggerFactory.getLogger(UserController.class);

//	@PostMapping(value = "/createandviewwholelist")
//	public List<User> persist(@RequestBody final User users) {
//		userRespository.save(users);
//		List<User> li = userRespository.findAll();
//		return li;
//	}

	/**
	 * Crete User and Return the whole POJO
	 * @param users
	 * @return
	 */
	@PostMapping(value = "/user")
	public ResponseEntity saveUser(@RequestBody final UserDto users) {
		User userWithDuplicateUsername = userRespository.findByName(users.getName());
		if(userWithDuplicateUsername != null) {
			throw new RuntimeException("Duplicate username.");
		}
//		User userWithDuplicateEmail = userRespository.findByEmail(users.getEmail());
//		if(userWithDuplicateEmail != null) {
//			throw new RuntimeException("Duplicate email.");
//		}
//		if(!Helper.validate(users.getEmail()))
//			throw new RuntimeException("Invalid Email");
//		if(users.getAge()<0 || users.getAge()>100){
//			throw new RuntimeException("Invalid Age");
//		}
		User user = new User();
		user.setName(users.getName());
		user.setAge(users.getAge());
		user.setEmail(users.getEmail());
		return new ResponseEntity(userRespository.save(user), HttpStatus.OK);
	}

	/**
	 * Fetch user by user id.:
	 * @param id
	 * @return
	 */

	@GetMapping("/user/{id}")
	public ResponseEntity getById(@PathVariable Integer id) {
		return new ResponseEntity(userRespository.findById(id).get(),HttpStatus.OK);
	}

	/**
	 * Get all User
	 * @return
	 */
	@GetMapping("/user")
	public ResponseEntity getUsers(@RequestParam Integer page, @RequestParam Integer count) {
		return new ResponseEntity(userRespository.findAll().stream().limit(count).collect(Collectors.toList()),HttpStatus.OK);
	}

	/**
	 * Upadate existing User
	 * 
	 * @param id
	 * @param users
	 * @return
	 */

	@PutMapping("/user/{id}")
	public ResponseEntity updateUser(@PathVariable Integer id, @RequestBody UserDto users) {
		User up=userRespository.findById(id).orElseThrow(()->new ResourceNotFound("Id not exist"));
//		if(userRespository.findByName(users.getName())!=null) {
//			throw new RuntimeException("Duplicate username.");
//		}
//		if(!Helper.validate(users.getEmail()))
//			throw new RuntimeException("Invalid Email");
//		if(users.getAge()<0 || users.getAge()>100){
//			throw new RuntimeException("Invalid Age");
//		}
		up.setName(users.getName());
		up.setAge(users.getAge());
		up.setUpdatedAt(new Date());
		return new ResponseEntity(userRespository.save(up),HttpStatus.OK);
	}

	@DeleteMapping("user/{id}")
	public ResponseEntity deleteUser(@PathVariable Integer id) {
		User up=userRespository.findById(id).orElseThrow(()->new ResourceNotFound("Id not exist"));
		userRespository.delete(up);
		return new ResponseEntity(HttpStatus.OK);
	}

	/***
	 * /RestAPI/user/?ids=80,81,82
	 * 
	 * @param id
	 * @return
	 */
	@DeleteMapping("user/")
	public ResponseEntity deleteUsers(@RequestParam("ids") List<Integer> id) {
		if(id.size()==0 || id==null || id.get(0)==0){
			userRespository.deleteAll();
			return new ResponseEntity(HttpStatus.OK);
		}
		for (Integer it : id) {
			User up = userRespository.findById(it).orElseThrow(() -> new ResourceNotFound("Id not exist"));
			userRespository.delete(up);
			continue;
		}
			return ResponseEntity.ok().build();
		}

}
