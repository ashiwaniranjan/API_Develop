package com.apis.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.apis.model.User;


public interface UserDao extends JpaRepository<User, Integer>{
    User findByName(String name);
    User findByEmail(String email);


}
